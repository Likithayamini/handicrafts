import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: any;

  constructor() {
    this.products = [
      {productId:1001, productName:'Jug', price:499.00,  imagePath:'assets/images/1001.jpeg', description:'Jug'},
      {productId:1002, productName:'Mat',  price:250.00,  imagePath:'assets/images/1002.jpeg', description:'Mat'},
      {productId:1003, productName:'Plate',    price:450.00,  imagePath:'assets/images/1003.jpeg', description:'Plate '},
      {productId:1004, productName:'Umbrella',  price:499.00,  imagePath:'assets/images/1004.jpeg', description:'Umbrella'},
      {productId:1005, productName:'Vase',     price:389.00, imagePath:'assets/images/1005.jpeg', description:'Vase'},
    ];
  }

  ngOnInit(): void {
  }

  addToCart(products: any): void {
    console.log(products);
  }

}
